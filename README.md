<h1>Angular - Lista użytkowników</h1>
<h2>Potrzebne pakiety globalne</h2>
<p>
Upenij się, że masz zainstalowany <code>angular-cli</code> oraz <code>typescript@2.7.2</code>, instalacja pakietów globalnych powinna się odbywać przez <code>sudo</code> jeżeli masz przejdź do punktu 2
</p>
<pre><code>$ sudo npm install -g @angular/cli
$ sudo npm install -g --save typescript@2.7.2
</code></pre>
<h2>Instrukcja uruchamiana</h2>
<pre><code>$ git clone https://gitlab.com/szymonzak/angular-users-list.git
$ cd angular-users-list
$ npm install
$ ng serve
</code>
</pre>
<p>
Jeżeli nie będzie żadnych błędów aplikacja powinna się uruchomić pod adresem <code>http://localhost:4200/</code>
</p>