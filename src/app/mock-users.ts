import { User } from './user';

export const USERS: User[] = [
  { id: 1, name: 'Jan', surname: 'Niezbędny' },
  { id: 2, name: 'Kamil', surname: 'Potrzebny' },
  { id: 3, name: 'Paweł', surname: 'Pomocny' },
  { id: 4, name: 'Konrad', surname: 'Konieczny' }
];