import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DialogComponent } from './dialog/dialog.component';
import { UserActionsComponent } from './user-actions/user-actions.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DialogComponent,
    UserActionsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
