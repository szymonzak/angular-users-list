import { Component, Input, Output, EventEmitter } from '@angular/core';
import { USER_ACTIONS } from '../mock-user-actions';
import { User } from '../user';

@Component({
  selector: 'app-user-actions',
  templateUrl: './user-actions.component.html',
  styleUrls: ['./user-actions.component.css']
})

export class UserActionsComponent {
	@Input() user: User;
	@Output() action: EventEmitter<object> = new EventEmitter<object>();

	userActions: Array<any> = USER_ACTIONS;

	createAction(user,action): void {
		this.action.emit({
			user: user,
			action: action
		});
	}
}
