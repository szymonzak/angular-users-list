export class Dialog {
  id: number;
  title: string;
  content: string;
}