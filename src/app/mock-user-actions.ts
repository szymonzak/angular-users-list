import { UserActions } from './user-actions';

export const USER_ACTIONS: UserActions[] = [
  { name: 'Edytuj', type: 'edit' },
  { name: 'Usuń', type: 'delete' }
];