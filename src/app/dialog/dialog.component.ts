import { Component, ViewChild, Directive } from '@angular/core';
import { Dialog } from '../dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})

@Directive({
  selector: '[appDialog]'
})

export class DialogComponent {
  @ViewChild('dialogElement') dialogElement;

  private decision: boolean;
  private resolve: any;
  private reject: any;
  private data: object = {};

  open(data: Dialog){
    this.data = data;
    this.dialogElement.nativeElement.showModal();

    return new Promise((resolve,reject): void => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }

  onDecision(decision): void {
    this.resolve(decision);

    this.dialogElement.nativeElement.close();
    this.data = {};
  }
}
