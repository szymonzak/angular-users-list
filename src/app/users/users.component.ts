import { Component, ViewChild } from '@angular/core';
import { DialogComponent } from '../dialog/dialog.component';
import { UserActionsComponent } from '../user-actions/user-actions.component';
import { User } from '../user';
import { USERS } from '../mock-users';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {
  @ViewChild('dialogElement') dialogElement;

  users: Array<any> = USERS;
  selectedUser: User;

  userAction(data): void {
  	this.selectedUser = data.user;

  	switch(data.action.type){
  		case 'delete':
		    this.dialogElement.open({
		      user: this.selectedUser,
		      title: 'Usuń użytkownika',
		      content: 'Czy na pewno chcesz usunąć użytkownika?'
		    }).then((response) => {
		    	if(response === true){
		      	this.users = this.users.filter(user => user.id !== this.selectedUser.id)
		      }
		    });
  		break;
  	}
  }
}
